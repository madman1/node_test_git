/// <reference path="typings/tsd.d.ts" />

var http = require('http');
var url = require('url');

var server = http.Server(function (req, res) {
   console.log(req.method, req.url);

   var urlParsed = url.parse(req.url, true);
   console.log(urlParsed);

   if(urlParsed.pathname == '/test')
   {
       res.render('react_test/index.html');
   }

   if(urlParsed.pathname == '/echo' && urlParsed.query.message)
   {
       res.statusCode = 200;
       res.end(urlParsed.query.message);
   }else{
      // res.statusCode = 404;
      // res.end('Page not found');
   }
});

server.listen(1337, 'localhost');