/// <reference path="typings/tsd.d.ts" />

var EventEmitter = require('events').EventEmitter;

var server = new EventEmitter();

server.on('request', function (request) {   // event handler
    request.aproved = true;
    console.log(request);
});

server.emit('request', {from: 'Клиент'}); // fires event. 1 param - event name,  2 param - context, will be passed in handlers
