

var User = require('./user');   // resuire('user') is also good. but it should be started as NODE_PATH node file.js
var log = require('./logger')(module); // path parameter and execute function
var util = require('util');  // takes from node_modules

function run() {
  var vasya = new User("Vasya");
  var petro = new User("Petro");
  vasya.hello(petro);    

  log('some text');
  var format = util.format('My format %s %d %j', "string", 5, {test:"fd"} );
  var str = format.toString();
  
}

if(module.parent){
    exports.run = run;
}
else{
    run();

}

